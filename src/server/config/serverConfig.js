module.exports = {
	version: '0.3.1',
	port: 4000,
	startupMessage: 'Server: ready',
	defaultZone: 'fjolarok'
};
